# -*- mode: ruby -*-
# vi: set ft=ruby :

ENV["LC_ALL"] = "en_US.UTF-8"

Vagrant.configure("2") do |config|

  # awx node
  config.vm.define "awx" do |awx|
    awx.vm.box = "debian/contrib-stretch64"
    awx.vm.hostname = "awx"
    awx.vm.network "private_network", ip: "10.0.0.1", virtualbox__intnet: "awx"
    awx.vm.network "forwarded_port", guest: 8080, host: 8888

    awx.vm.provider "virtualbox" do |v|
      v.memory = 2048
      v.cpus = 1
    end

    awx.vm.provision "ansible_local" do |a|
      a.playbook = "awx.yml"
      a.install_mode = "pip"
      a.version = "2.7.5"
    end

    awx.vm.provision "shell" do |s|
      s.inline = "ansible-playbook -i /vagrant/inventory /vagrant/awx/installer/install.yml"
    end
  end

  # test nodes
  (1..3).each do |i|
    config.vm.define "node-#{i}" do |node|
      node.vm.box = "debian/contrib-stretch64"
      node.vm.hostname = "node-#{i}"
      node.vm.network "private_network", ip: "10.0.0.#{i+1}" , virtualbox__intnet: "awx"

      node.vm.provider "virtualbox" do |v|
        v.memory = 128
        v.cpus = 1
      end

      node.vm.provision "ansible_local" do |a|
        a.playbook = "nodes.yml"
        a.install_mode = "pip"
        a.version = "2.7.5"
      end
    end
  end
end
